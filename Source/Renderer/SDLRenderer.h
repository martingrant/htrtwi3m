#pragma once

#include <string>
#include <memory>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_ttf.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
        #include "SDL_ttf.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
        #include <SDL2_ttf/SDL_ttf.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#endif

#include "../ServicesManager/ServicesManager.h"
#include "../Game/HUD/Text.h"

class SDLRenderer
{
	/* Constructors & Destructors */
public:
	SDLRenderer(SDL_Window* window, std::shared_ptr<ServicesManager> servicesManager);
	~SDLRenderer();

public:
	void renderTexture(SDL_Texture* sourceTexture, const SDL_Rect* sourceRect, SDL_Rect* destinationRect);
	void renderTexture(std::string textureName, const SDL_Rect* sourceRect, SDL_Rect* destinationRect);
	void renderRect(const SDL_Rect* rect, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha);
	void setTextureBlendColour(std::string textureName, Uint8 red, Uint8 green, Uint8 blue);
	void renderText(Text* text);

	SDL_Renderer* getRenderer();

private:
	SDL_Renderer* m_renderer;
	std::shared_ptr<ServicesManager> m_servicesManager;
};

