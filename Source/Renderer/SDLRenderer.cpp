#include "SDLRenderer.h"


SDLRenderer::SDLRenderer(SDL_Window* window, std::shared_ptr<ServicesManager> servicesManager)
{
	m_renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
	SDL_SetRenderDrawColor(m_renderer, 100, 100, 50, SDL_ALPHA_OPAQUE);

	m_servicesManager = servicesManager;




}


SDLRenderer::~SDLRenderer()
{
	SDL_DestroyRenderer(m_renderer);
}


void SDLRenderer::renderTexture(SDL_Texture* sourceTexture, const SDL_Rect* sourceRect, SDL_Rect* destinationRect)
{
	SDL_RenderCopy(m_renderer, sourceTexture, sourceRect, destinationRect);
}


void SDLRenderer::renderTexture(std::string textureName, const SDL_Rect* sourceRect, SDL_Rect* destinationRect)
{
	SDL_RenderCopy(m_renderer, m_servicesManager->getResourceManager()->getTexture(textureName), sourceRect, destinationRect);
}


void SDLRenderer::renderRect(const SDL_Rect* rect, Uint8 red, Uint8 green, Uint8 blue, Uint8 alpha)
{
	SDL_SetRenderDrawColor(m_renderer, red, green, blue, alpha);
	SDL_RenderFillRect(m_renderer, rect);
	SDL_RenderDrawRect(m_renderer, rect);
}


void SDLRenderer::setTextureBlendColour(std::string textureName, Uint8 red, Uint8 green, Uint8 blue)
{
	// 255, 255, 255 for normal texture colour
	SDL_SetTextureColorMod(m_servicesManager->getResourceManager()->getTexture(textureName), red, green, blue);
}


void SDLRenderer::renderText(Text* text)
{
//	TTF_Font *font = TTF_OpenFont(text->getFont().c_str(), text->getSize());
//	SDL_Colour test = { 240, 240, 240 };
//	SDL_Surface *surf = TTF_RenderText_Blended(font, text->getText().c_str(), text->getColour());
//
//	SDL_Rect rect;
//	rect.x = text->getPositionX();
//	rect.y = text->getPositionY();
//	rect.w = surf->w;
//	rect.h = surf->h;
//
//	SDL_Texture *texture = SDL_CreateTextureFromSurface(m_renderer, surf);
//
//	SDL_FreeSurface(surf);
//	TTF_CloseFont(font);
//
//	renderTexture(texture, NULL, &rect);
}


SDL_Renderer* SDLRenderer::getRenderer()
{
	return m_renderer;
}
