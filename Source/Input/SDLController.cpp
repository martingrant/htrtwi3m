#include "SDLController.h"


#pragma region Class Constructor & Destructor
/*
* Constructs the Controller class.
*/
SDLController::SDLController()
{
	m_controllerStatus = false;

	std::cout << "Constructed SDLController successfully. " << std::endl << std::endl;
}

/*
* Destructs the Controller class.
*/
SDLController::~SDLController(void)
{
	std::cout << "Destructing SDLController... " << std::endl << std::endl;

	closeController();

	m_controller = nullptr;
	m_haptic = nullptr;

	std::cout << "Destructed SDLController successfully. " << std::endl << std::endl;
}

#pragma endregion


#pragma region Manage Controller Device
/*
* Sets the ID of the controller object.
*
* @param int controllerID - the ID to be set for the controller.
*/
void SDLController::setControllerID(int controllerID)
{
	m_controllerID = controllerID;
}

/*
* Opens the controller to be used for querying.
*
* return bool - returns true or false if the controller was opened.
*/
bool SDLController::openController()
{
	bool status = false;

	// Check if index is a supported device
	if (SDL_IsGameController(m_controllerID))
	{
		// Test to open controller, if true open it's haptic if supported
		if ((m_controller = SDL_GameControllerOpen(m_controllerID)))
		{
			// Print opened controller information
			std::cout << "Controller opened on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;;
			status = true;
			m_controllerStatus = true;

			//openHaptic();
		}
		else
		{
			// Print information if controller failed to open
			std::cout << "Controller on index: " << m_controllerID << " failed to open. Device name:  " << SDL_GameControllerName(m_controller) << std::endl << std::endl;
			status = false;
		}
	}
	else
	{
		std::cout << "Device unsupported on index: " << m_controllerID << " failed to open." << std::endl << std::endl;
		status = false;
	}

	return status;
}

/*
* Closes the controller.
*
* @return bool - true or false if controller was closed or not.
*/
bool SDLController::closeController()
{
	bool status = false;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetAttached(m_controller))
		{
			// Close haptic and then controller
			//closeHaptic();
			SDL_GameControllerClose(m_controller);

			status = true;
			m_controllerStatus = false;

			// Print controller information
			std::cout << "Controller closed on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;
		}
		else
		{
			// If controller not found, print information about controller index
			std::cout << "Close controller failed. Device not found on index: " << m_controllerID << ". Device name: " << SDL_GameControllerName(m_controller) << std::endl << std::endl;

			status = false;
		}
	}

	return status;
}

/*
* Returns the status of a controller declaring if it is connected or not.
*
* @return bool - true or false if controller is connected or not.
*/
bool SDLController::getControllerStatus()
{
	if (SDL_GameControllerGetAttached(m_controller) == false)
	{
		m_controllerStatus = false;
	}
	else
	{
		m_controllerStatus = true;
	}

	return m_controllerStatus;
}


/*
* Open the haptic on the controller device.
*
* @return bool status - true or false if haptic was opened or not.
*/
bool SDLController::openHaptic()
{
	bool status = false;

	// Create temp joystick pointer
	SDL_Joystick* tempJoystick = nullptr;

	// Exit if unable to access joystick device
	if (!SDL_JoystickOpen(m_controllerID))
	{
		std::cout << "Unable to access device on index: " << m_controllerID << ". Device name: " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		return false;
	}
	else tempJoystick = SDL_JoystickOpen(m_controllerID);

	// Check if index is a supported device
	if (SDL_JoystickIsHaptic(tempJoystick))
	{
		// Point temp pointer to device index and open/init haptic
		if ((m_haptic = SDL_HapticOpen(m_controllerID)))
		{
			// Print opened haptic information
			std::cout << "Haptic opened on device on index: " << m_controllerID << std::endl << "Haptic: "
				<< SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			status = true;

			// Init rumble on haptic
			if (SDL_HapticRumbleSupported(m_haptic))
			{
				SDL_HapticRumbleInit(m_haptic);
				std::cout << "Rumble initialized for haptic on device index: " << m_controllerID << std::endl << "Haptic: " <<
					SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			}
			// Rumble not supported
			else
			{
				std::cout << "Rumble not supported for haptic on device index: " << m_controllerID << std::endl << "Haptic: " <<
					SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
			}
		}
		// Couldn't open haptic
		else
		{
			std::cout << "Haptic for index on index: " << m_controllerID << " failed to open. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
			status = false;
		}
	}
	// Device ain't got no haptic feedback
	else
	{
		std::cout << "Haptic not supported for device on index: " << m_controllerID << " failed to open. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		status = false;
	}

	// Close joystick device (only needed for this method)
	SDL_JoystickClose(tempJoystick);
	tempJoystick = nullptr;

	return status;
}

/*
* Closes haptic device.
*
* @return bool status - true or false if haptic was closed or not.
*/
bool SDLController::closeHaptic()
{
	bool status = false;

	// Check if index is a supported device
	if (SDL_HapticOpened(m_controllerID) == 1)
	{
		SDL_HapticStopAll(m_haptic);
		SDL_HapticClose(m_haptic);

		// Print haptic information
		std::cout << "Haptic closed for device on index: " << m_controllerID << std::endl << "Haptic: "
			<< SDL_HapticName(m_controllerID) << " on device: " << SDL_GameControllerNameForIndex(m_controllerID) << std::endl << std::endl;
		status = true;
	}
	else
	{
		std::cout << "Haptic for device on index: " << m_controllerID << " failed to close. Device name:  " << SDL_JoystickNameForIndex(m_controllerID) << std::endl << std::endl;
		status = false;
	}

	return status;
}

#pragma endregion


#pragma region Controller Data Methods 
/*
* Returns Axis value from controller.
*
* @param const char* axis - the axis on the controller to be queried.
* @return int axisValue - the value of the axis specified to be queried.
*/
float SDLController::getControllerAxis(const char* axis)
{
	float axisValue = 0;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetAxis(m_controller, SDL_GameControllerGetAxisFromString(axis)))
		{
			axisValue = SDL_GameControllerGetAxis(m_controller, SDL_GameControllerGetAxisFromString(axis));
			axisValue = axisValue / 32767;

			//std::cout << "Axis pushed on controller: " << controllerID << " (" << SDL_GameControllerNameForIndex(controllerID) << "): '"
			//<< SDL_GameControllerGetStringForAxis((SDL_GameControllerAxis) axis) << " ( " 
			//<< SDL_GameControllerGetAxis(tempController, (SDL_GameControllerAxis) axis) << " )'" << std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return axisValue;
}


float SDLController::getControllerAxis(SDL_GameControllerAxis axis)
{
	float axisValue = 0;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetAxis(m_controller, axis))
		{
			axisValue = SDL_GameControllerGetAxis(m_controller, axis);
			axisValue = axisValue / 32767;

			//std::cout << "Axis pushed on controller: " << controllerID << " (" << SDL_GameControllerNameForIndex(controllerID) << "): '"
			//<< SDL_GameControllerGetStringForAxis((SDL_GameControllerAxis) axis) << " ( " 
			//<< SDL_GameControllerGetAxis(tempController, (SDL_GameControllerAxis) axis) << " )'" << std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return axisValue;
}


/*
* Returns boolean based on controller button being pressed.
*
* @param const char* button - the button on the controller to be queried.
* @return bool status - true or false if button was pressed or not.
*/
bool SDLController::getControllerButtonState(const char* button)
{
	bool status = false;

	// Check if controller is connected and opened
	if (m_controllerStatus == true)
	{
		if (SDL_GameControllerGetButton(m_controller, SDL_GameControllerGetButtonFromString(button)))
		{
			status = true;

			//std::cout << "Button pressed on controller: " << m_controllerID << " (" << SDL_GameControllerNameForIndex(m_controllerID) 
			//<< "): " << "'" << SDL_GameControllerGetStringForButton((SDL_GameControllerButton) button) << "'"<< std::endl;
		}
	} //else std::cout << "Controller on index: " << m_controllerID << " not found or connected" << std::endl;

	return status;
}

#pragma endregion

