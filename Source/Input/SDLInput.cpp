#include "SDLInput.h"


#pragma region Class Constructor & Destructor
/*
* Constructs SDLInput class.
* Inits SDL subsystems for Game Controllers and Haptics.
* Detects number of controllers connected and create a new Controller object for each and opens each.
*/
SDLInput::SDLInput(void)
{
	std::cout << "Constructing SDLInput..." << std::endl << std::endl;

	if (SDL_InitSubSystem(SDL_INIT_GAMECONTROLLER | SDL_INIT_HAPTIC) != 0)
	{
		std::cout << "SDL_InitSubSystem: " << SDL_GetError() << std::endl;
	}

	// Call detect controllers to find out how many controlers are currently connected	
	m_connectedControllers = detectControllers();

	// Detect number of haptic devices;
	//detectHaptics();

	// Add a new controller to the controller map and open it for how many controllers have been detected
	for (int index = 0; index < m_connectedControllers; ++index)
	{
		m_controllerList[index] = SDLController();
		m_controllerList[index].setControllerID(index);
		openController(index);
	}

	std::cout << "Constructed SDLInput successfully." << std::endl << std::endl;
}

/*
* Destructs SDLInput.
*/
SDLInput::~SDLInput(void)
{
	std::cout << "Destructed SDLInput successfully." << std::endl << std::endl;
}

#pragma endregion


#pragma region General Public Methods
/*
* Updates SDL event identifier.
* This should be called in game loop.
*/
void SDLInput::update()
{
	SDL_PollEvent(&m_SDLEvent);
}

#pragma endregion


#pragma region Keyboard Methods
/*
* Returns a boolean based on state of specified key parameter
*
* @param SDL_scancode scanCode - Scan code identifier of a keyboard key e.g. SDL_SCANCODE_SPACE for space bar
* @return status - set to true or false if the specified key has been pressed or not
*/
bool SDLInput::getKeyState(const char* key)
{
	bool status = false;

	//char* scanCode = strcat("SDL_Scancode", key);

	const Uint8* keyboard = SDL_GetKeyboardState(NULL);

	if (keyboard[SDL_GetScancodeFromName(key)]) {
		status = true;
		//std::cout << "Key pressed: " << SDL_GetScancodeName(scanCode) << " (Scancode: " << scanCode << ")" << std::endl; 
		
	}

	return status;
}

#pragma endregion


#pragma region Mouse Methods 
/*
* Returns the current position of the mouse.
*
* @return std::pair<int, int> - A pair of int values for the current position of the mouse.
*/
std::pair<int, int> SDLInput::getMousePosition()
{
	int x, y;
	SDL_GetMouseState(&x, &y);

	return std::pair<int, int>(x, y);
}

/*
* Returns the current relative position of the mouse (distance from last position).
*
* @return std::pair<int, int> - A pair of int values for the current relative position of the mouse.
*/
std::pair<int, int> SDLInput::getMouseRelativePosition()
{
	int x, y;
	SDL_GetRelativeMouseState(&x, &y);

	return std::pair<int, int>(x, y);
}

/*
* Returns a boolean based on the current state of a mouse button.
*
* @param unsigned int buttonID - identifier for any mouse button.
* @return bool - true or false if the specified mouse button has been pressed or not.
*/
bool SDLInput::mouseButtonState(unsigned int buttonID)
{
	if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON(buttonID))
	{
		return true;
	}
	else
	{
		return false;
	}
}

/*
* Returns an int corresponding to the current position of the mouse wheel.
*
* @return int - current value of the mouse scroll wheel.
*/
int SDLInput::getMouseWheel()
{
	int wheelValue = 0;

	while (SDL_PollEvent(&m_SDLEvent)) {
		if (m_SDLEvent.type == SDL_MOUSEWHEEL) {
			std::cout << "Mouse wheel scrolled: " << m_SDLEvent.wheel.y << std::endl;
			wheelValue = m_SDLEvent.wheel.x;
		}
	}

	return wheelValue;
}

#pragma endregion


#pragma region Controller Methods
/*
* Returns Axis value from specified controller.
*
* @param unsigned int ControllerID - the controller to be queried.
* @param const char* axis - the axis on the controller to be queried.
* @return int axisValue - the value of the axis specified to be queried.
*/
float SDLInput::getControllerAxis(unsigned int controllerID, const char* axis)
{
	float axisValue = 0;

	if (controllerID < m_connectedControllers)
    {
		axisValue = m_controllerList[controllerID].getControllerAxis(axis);
    }

	return axisValue;
}


float SDLInput::getControllerAxis(unsigned int controllerID, SDL_GameControllerAxis axis)
{
	float axisValue = 0;

	if (controllerID < m_connectedControllers)
	{
		axisValue = m_controllerList[controllerID].getControllerAxis(axis);
	}

	return axisValue;
}


/*
* Returns boolean based on state of Controller button.
*
* @param unsigned int ControllerID - the controller to be queried.
* @param const char* button - the button on the controller to be queried.
* @return bool buttonState - true or false if the button has been pressed or not.
*/
bool SDLInput::getControllerButtonState(unsigned int controllerID, const char* button)
{
	bool buttonState = false;

	if (controllerID < m_connectedControllers)
    {
		buttonState = m_controllerList[controllerID].getControllerButtonState(button);
    }

	return buttonState;
}

/*
* Returns boolean if Controller button has been released/is up/is not held down.
*
* @param unsigned int ControllerID - the controller to be queried.
* @param const char* button - the button on the controller to be queried.
* @return - bool buttonState - true or false if button is up.
*/
bool SDLInput::onControllerButtonUp(unsigned int controllerID, const char* button)
{
	bool buttonState = false;

	if (controllerID < m_connectedControllers)
	{
		if (m_SDLEvent.cbutton.type == SDL_CONTROLLERBUTTONUP)
		{
			if (m_SDLEvent.cbutton.button == SDL_GameControllerGetAxisFromString(button))
			{
				buttonState = true;
			}
		}
	}

	return buttonState;
}

/*
* Returns boolean if Controller button has is down.
*
* @param unsigned int ControllerID - the controller to be queried.
* @param const char* button - the button on the controller to be queried.
* @return - bool buttonState - true or false if button is down.
*/
bool SDLInput::onControllerButtonDown(unsigned int controllerID, const char* button)
{
	bool buttonState = true;

	if (controllerID < m_connectedControllers)
	{
		if (m_controllerList[controllerID].getControllerButtonState(button) == false)
		{
			buttonState = false;
		}
		else
		{
			buttonState = true;
		}
	}

	return buttonState;
}

/*
* Detects the number of controllers connected to the system currently.
*
* @return int connectedControllers - the number of connected controllers.
*/
int SDLInput::detectControllers()
{
	int connectedControllers = SDL_NumJoysticks();

	//std::cout << std::endl << m_connectedControllers << " controller(s) detected." << std::endl;

	// Print number of controllers and their type if any are detected
	//if (m_connectedControllers > 0)
	//	for (int index = 0; index < m_connectedControllers; index++)
	//	std::cout << "Controller detected on index: " << index << ". Device name: " << SDL_GameControllerNameForIndex(index) << std::endl;

	return connectedControllers;
}

/*
* Detect the number of haptic devices connected to the system.
*/
void SDLInput::detectHaptics()
{
	int numHaptics = SDL_NumHaptics();

	std::cout << numHaptics << " Haptic device(s) detected." << std::endl;

	// Print number of haptics and their type if any are detected
	if (numHaptics > 0)
	{
		for (int i = 0; i < numHaptics; ++i)
		{
			std::cout << "Haptic detected on device on index: " << i << ". Device name: " << SDL_HapticName(i) << std::endl;
		}
	}
}

/*
* Opens a controller on the specified index.
*
* @param unsigned int controllerID - the controller to be opened.
* @return bool - true or false if the specified controller has been opened.
*/
bool SDLInput::openController(unsigned int controllerID)
{
	return m_controllerList[controllerID].openController();
}

/*
* Closes a controller on the specified index.
*
* @param unsigned int controllerID - the controller to be closed.
* @return bool - true or false if the specified controller has been closed.
*/
bool SDLInput::closeController(unsigned int controllerID)
{
	return m_controllerList[controllerID].closeController();
}

/*
* Polls all opened controllers to check if any have been removed from the system.
* Polls for any new controllers being connected and will open them.
*/
void SDLInput::pollControllers()
{
	// Test if controllers are removed, try to close them
	if (m_SDLEvent.cdevice.type == SDL_CONTROLLERDEVICEREMOVED)
	{
		for (std::unordered_map<int, SDLController>::iterator iterator = m_controllerList.begin(); iterator != m_controllerList.end(); ++iterator)
		{
			if (/*!iterator->second &&*/ !iterator->second.getControllerStatus())
			{
				std::cout << "Controller " << iterator->second.getControllerID() << " disconneted." << std::endl;
				iterator->second.closeController();
			}
		}
	}

	// Check if controllers are connected, try to open them
	if (m_SDLEvent.cdevice.type == SDL_CONTROLLERDEVICEADDED)
	{
		for (std::unordered_map<int, SDLController>::iterator iterator = m_controllerList.begin(); iterator != m_controllerList.end(); ++iterator)
		{
			if (/*!iterator->second && */!iterator->second.getControllerStatus())
			{
				std::cout << "Controller " << iterator->second.getControllerID() << " connected." << std::endl;
				iterator->second.openController();
			}
		}
	}
}

#pragma endregion