#include "ResourceManager.h"


#pragma region Constructors & Destructors

ResourceManager::ResourceManager(SDL_Renderer* renderer)
{
	m_renderer = renderer;

	m_audioRate = 22050;
	m_audioFormat = AUDIO_S16SYS;
	m_audioChannels = 2;
	m_audioBuffers = 4096;

	if (Mix_OpenAudio(m_audioRate, m_audioFormat, m_audioChannels, m_audioBuffers) != 0) {
		fprintf(stderr, "Unable to initialize audio: %s\n", Mix_GetError());
	}
}


ResourceManager::~ResourceManager()
{
	for (auto iterator = m_textureMap.begin(); iterator != m_textureMap.end(); ++iterator)
	{
		SDL_DestroyTexture(iterator->second);
	}

	m_textureMap.clear();
}

#pragma endregion

#pragma region General Resource Management

void ResourceManager::loadResources()
{


//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        loadTexture("MainMenuBG", "gamestartscreen.png");
//        loadTexture("CreditsBG", "creditsscreen.png");
//        loadTexture("StartButton", "startbutton.png");
//        loadTexture("CreditsButton", "credits.png");
//        loadTexture("BackButton", "backbutton.png");
//        loadTexture("GameBG", "environment_003.png");
//
//        loadTexture("TowerNeutral", "TowerNeutral.png");
//        loadTexture("TowerIndicator", "circle.png");
//
//        loadTexture("BluePlayer", "blue_run_animation_001.png");
//        loadTexture("GreenPlayer", "green_run_animation_001.png");
//        loadTexture("PinkPlayer", "pink_run_animation_001.png");
//        loadTexture("YellowPlayer", "yellow_run_animation_001.png");
//
//        loadMusic("IntroLoop", "introloop.wav");
//        loadMusic("Music", "musicedited.wav");
//        loadMusic("Tower", "towereffect.wav");
//    }
//    else
//    {

    loadTexture("MainMenuBG", "../Resources/Textures/Menus/gamestartscreen.png");
	loadTexture("CreditsBG", "../Resources/Textures/Menus/creditsscreen.png");
	loadTexture("StartButton", "../Resources/Textures/Menus/startbutton.png");
	loadTexture("CreditsButton", "../Resources/Textures/Menus/credits.png");
	loadTexture("BackButton", "../Resources/Textures/Menus/backbutton.png");
	loadTexture("GameBG", "../Resources/Textures/Backgrounds/environment_003.png");

	loadTexture("TowerNeutral", "../Resources/Textures/Towers/TowerNeutral.png");
	loadTexture("TowerIndicator", "../Resources/Textures/Towers/circle.png");

	loadTexture("BluePlayer", "../Resources/Textures/Characters/Blue/blue_run_animation_001.png");
	loadTexture("GreenPlayer", "../Resources/Textures/Characters/Green/green_run_animation_001.png");
	loadTexture("PinkPlayer", "../Resources/Textures/Characters/Pink/pink_run_animation_001.png");
	loadTexture("YellowPlayer", "../Resources/Textures/Characters/Yellow/yellow_run_animation_001.png");

	loadMusic("IntroLoop", "../Resources/Audio/introloop.wav");
	loadMusic("Music", "../Resources/Audio/musicedited.wav");
	loadMusic("Tower", "../Resources/Audio/towereffect.wav");
    //}
}

#pragma endregion


#pragma region Textures

void ResourceManager::loadTexture(std::string name, std::string filePath)
{
	m_textureMap[name] = IMG_LoadTexture(m_renderer, filePath.c_str());
}


SDL_Texture* ResourceManager::getTexture(std::string name)
{
	return m_textureMap[name];
}

#pragma endregion

#pragma region Audio

void ResourceManager::loadMusic(std::string name, std::string filePath)
{
	m_audioMap[name] = Mix_LoadMUS(filePath.c_str());
}


void ResourceManager::playMusic(std::string name, int numberOfLoops)
{
	Mix_PlayMusic(m_audioMap[name], numberOfLoops);
}


void ResourceManager::stopMusic()
{
	Mix_HaltMusic();
}


#pragma endregion
