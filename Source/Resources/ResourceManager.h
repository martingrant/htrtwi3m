#pragma once

#include <memory>
#include <unordered_map>
#include <iostream>
#include <vector>
#include <string>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>
#define TARGET_IPHONE_SIMULATOR 0
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL_image.h"
        #include "SDL_mixer.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
        #include <SDL2_image/SDL_image.h>
        #include <SDL2_mixer/SDL_mixer.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_mixer.h>
#endif


class ResourceManager
{
	/* Constructors & Destructors */
public:
	ResourceManager(SDL_Renderer* renderer);
	~ResourceManager();
	/* General Resource Management*/
public:
	void loadResources();

	/* Textures */
public:
	void loadTexture(std::string name, std::string filePath);
	SDL_Texture* getTexture(std::string name);

private:
	std::unordered_map<std::string, SDL_Texture*> m_textureMap;
	SDL_Renderer* m_renderer;

	/* Audio */
public:
	void loadMusic(std::string name, std::string filePath);
	void playMusic(std::string name, int numberOfLoops);
	void stopMusic();

private:
	std::unordered_map<std::string, Mix_Music*> m_audioMap;

	int m_audioRate;
	Uint16 m_audioFormat;
	int m_audioChannels;
	int m_audioBuffers;
};

