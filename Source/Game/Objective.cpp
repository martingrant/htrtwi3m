#include "Objective.h"


Objective::Objective(std::string currentTextureName, std::string indicatorTexture, unsigned int positionX, unsigned int positionY, unsigned int width, unsigned int height, unsigned int indicatorX, unsigned int indicatorY, unsigned int indicatorWidth, unsigned int indicatorHeight)
{
	m_currentTextureName = currentTextureName;
	m_indicatorTexture = indicatorTexture;

	m_objectiveRect.x = positionX;
	m_objectiveRect.y = positionY;
	m_objectiveRect.w = width;
	m_objectiveRect.h = height;

	m_indicatorRect.x = indicatorX;
	m_indicatorRect.y = indicatorY;
	m_indicatorRect.w = indicatorWidth;
	m_indicatorRect.h = indicatorHeight;
}


Objective::~Objective()
{
}
