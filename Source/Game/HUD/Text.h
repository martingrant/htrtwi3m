#pragma once

#include <string>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>>
#endif

class Text
{
public:
	Text() {}
	Text(std::string text, std::string font, unsigned int size, SDL_Color colour, unsigned int positionX, unsigned int positionY);
	~Text();

	void setText(std::string text);
	std::string getText();
	SDL_Color getColour();
	std::string getFont();
	unsigned int getSize();

	unsigned int getPositionX();
	unsigned int getPositionY();

private:
	std::string m_text;
	std::string m_font;
	unsigned int m_size;
	SDL_Color m_colour;

	unsigned int m_positionX;
	unsigned int m_positionY;
};

