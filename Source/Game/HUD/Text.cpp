#include "Text.h"


Text::Text(std::string text, std::string font, unsigned int size, SDL_Color colour, unsigned int positionX, unsigned int positionY)
{
	m_text = text;
	m_font = font;
	m_size = size;

	m_colour = colour;

	m_positionX = positionX;
	m_positionY = positionY;

}


Text::~Text()
{
}


void Text::setText(std::string text)
{ 
	m_text = text; 
}


std::string Text::getText()
{ 
	return m_text; 
}


SDL_Color Text::getColour()
{
	return m_colour; 
}


std::string Text::getFont()
{ 
	return m_font; 
}


unsigned int Text::getSize()
{ 
	return m_size;
}

unsigned int Text::getPositionX()
{ 
	return m_positionX; 
}


unsigned int Text::getPositionY()
{
	return m_positionY;
}