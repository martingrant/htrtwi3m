#pragma once

#include <unordered_map>

#ifdef _WIN32
#include <SDL.h>
#include <SDL_ttf.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
        #include "SDL_ttf.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
        #include <SDL2_ttf/SDL_ttf.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>>
#include <SDL2/SDL_ttf.h>
#endif

#include "Text.h"

class HUD
{
	/* Constructors & Destructors */
public:
	HUD();
	~HUD();

	void addText(std::string name, std::string text, std::string font, unsigned int size, SDL_Color colour, unsigned int positionX, unsigned int positionY);
	void setText(std::string name, std::string text);
	Text* getText(std::string name) { return &m_textMap[name]; }

private:
	std::unordered_map<std::string, Text> m_textMap;
};

