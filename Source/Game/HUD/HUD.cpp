#include "HUD.h"


HUD::HUD()
{
	TTF_Init();
}


HUD::~HUD()
{
}


void HUD::addText(std::string name, std::string text, std::string font, unsigned int size, SDL_Color colour, unsigned int positionX, unsigned int positionY)
{
	m_textMap[name] = Text(text, font, size, colour, positionX, positionY);
}


void HUD::setText(std::string name, std::string text)
{
	m_textMap[name].setText(text);
}