#include "Player.h"


#pragma region Constructors & Destructors

Player::Player(unsigned int playerID, std::string currentTextureName, unsigned int positionX, unsigned int positionY, unsigned int width, unsigned int height)
{
	m_playerID = playerID;
	m_currentTextureName = currentTextureName;

	m_rect.x = positionX;
	m_rect.y = positionY;
	m_rect.w = width;
	m_rect.h = height;

	m_score = 0;
}


Player::~Player()
{
}

#pragma endregion


void Player::setCurrentTextureName(std::string currentTextureName)
{ 
	m_currentTextureName = currentTextureName; 
}


std::string Player::getCurrentTextureName()
{ 
	return m_currentTextureName; 
}


SDL_Rect& Player::getRect()
{
	return m_rect; 
}


void Player::setScore(int score)
{
	m_score = score;
}


int Player::getScore()
{
	return m_score;
}