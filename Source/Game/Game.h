#pragma once

#include <vector>
#include <memory>

#include "Platform.h"
#include "Objective.h"
#include "Player.h"
#include "HUD/HUD.h"
#include "../Renderer/SDLRenderer.h"
#include "../ServicesManager/ServicesManager.h"

class Game
{
	/* Constructors & Destructors */
public:
	Game() {}
	Game(std::shared_ptr<SDLRenderer> renderer, std::shared_ptr<ServicesManager> servicesManager);
	~Game();

public:
	void update();
	void render();

	void start();
	void roundOver();

private:
	std::shared_ptr<SDLRenderer> m_renderer;
	std::shared_ptr<ServicesManager> m_servicesManager;

	std::vector<Platform> m_platformVector;
	std::vector<Objective> m_objectiveVector;
	std::vector<Player> m_playerVector;
	HUD m_HUD;

	unsigned int m_currentRound;
	unsigned int m_maxRounds;
};

