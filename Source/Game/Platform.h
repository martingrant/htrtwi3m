#pragma once

#include <string>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>>
#endif

class Platform
{
	/* Constructors & Destructors */
public:
	Platform(unsigned int positionX, unsigned int positionY, unsigned int width, unsigned int height);
	~Platform();

public:
	SDL_Rect& getRect() { return m_rect; }

private:
	SDL_Rect m_rect;
};

