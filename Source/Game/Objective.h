#pragma once

#include <string>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>>
#endif

class Objective
{
	/* Constructors & Destructors */
public:
	Objective(std::string currentTextureName, std::string indicatorTexture, unsigned int positionX, unsigned int positionY, unsigned int width, unsigned int height, unsigned int indicatorX, unsigned int indicatorY, unsigned int indicatorWidth, unsigned int indicatorHeight);
	~Objective();

public:
	SDL_Rect& getObjectiveRect() { return m_objectiveRect; }
	SDL_Rect& getIndicatorRect() { return m_indicatorRect; }
	std::string getCurrentTextureName() { return m_currentTextureName; }
	void setCurrentTextureName(std::string currentTextureName) { m_currentTextureName = currentTextureName; }
	std::string getIndicatorTexture() { return m_indicatorTexture; }

private:
	std::string m_currentTextureName;
	std::string m_indicatorTexture;
	SDL_Rect m_objectiveRect;
	SDL_Rect m_indicatorRect;
};

