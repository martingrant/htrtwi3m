#include "Platform.h"


#pragma region Constructors & Destructors

Platform::Platform(unsigned int positionX, unsigned int positionY, unsigned int width, unsigned int height)
{
	m_rect.x = positionX;
	m_rect.y = positionY;
	m_rect.w = width;
	m_rect.h = height;
}


Platform::~Platform()
{
}

#pragma endregion

