#include "Game.h"


#pragma region Constructors & Destructors

Game::Game(std::shared_ptr<SDLRenderer> renderer, std::shared_ptr<ServicesManager> servicesManager)
{
	m_renderer = renderer;
	m_servicesManager = servicesManager;

	m_platformVector.push_back(Platform(0, 710, 1280, 10));		// ground

	m_platformVector.push_back(Platform(10, 550, 180, 10));		// bottom left
	m_platformVector.push_back(Platform(370, 550, 180, 10));	// bottom centre left
	m_platformVector.push_back(Platform(730, 550, 180, 10));	// bottom centre right
	m_platformVector.push_back(Platform(1090, 550, 180, 10));	// bottom right

	m_platformVector.push_back(Platform(190, 390, 180, 10));	// mid left
	m_platformVector.push_back(Platform(550, 390, 180, 10));	// mid mid
	m_platformVector.push_back(Platform(910, 390, 180, 10));	// mid right

	m_platformVector.push_back(Platform(370, 230, 180, 10));	// top left
	m_platformVector.push_back(Platform(730, 230, 180, 10));	// top right

	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 60, 500, 50, 50, 75, 470, 20, 20));	// bottom left
	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 420, 500, 50, 50, 435, 470, 20, 20));	// bottom centre left
	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 780, 500, 50, 50, 795, 470, 20, 20));	// bottom centre right
	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 1140, 500, 50, 50, 1155, 470, 20, 20));	// bottom right

	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 240, 330, 60, 60, 260, 300, 20, 20));	// mid left
	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 600, 320, 70, 70, 625, 290, 20, 20));	// mid centre
	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 960, 330, 60, 60, 980, 300, 20, 20));	// mid right

	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 420, 170, 60, 60, 440, 140, 20, 20));	// top left
	m_objectiveVector.push_back(Objective("TowerNeutral", "TowerIndicator", 780, 170, 60, 60, 800, 140, 20, 20));	// top right

	m_playerVector.push_back(Player(0, "BluePlayer", 200, 642, 35, 48));
	m_playerVector.push_back(Player(1, "GreenPlayer", 300, 642, 35, 48));
	m_playerVector.push_back(Player(2, "PinkPlayer", 400, 642, 35, 48));
	m_playerVector.push_back(Player(3, "YellowPlayer", 500, 642, 35, 48));

	m_HUD = HUD();
	SDL_Color time = { 255, 0, 0 };

//    if (TARGET_IPHONE_SIMULATOR)
//    {
//        m_HUD.addText("Time", "0", "MONOFONT.ttf", 30, time, 550, 10);
//
//        SDL_Color score = { 255, 255, 255 };
//        m_HUD.addText("Player1Score", "P1: " + std::to_string(m_playerVector[0].getScore()), "MONOFONT.ttf", 30, score, 20, 10);
//        m_HUD.addText("Player2Score", "P2: " + std::to_string(m_playerVector[1].getScore()), "MONOFONT.ttf", 30, score, 300, 10);
//        m_HUD.addText("Player3Score", "P3: " + std::to_string(m_playerVector[2].getScore()), "MONOFONT.ttf", 30, score, 850, 10);
//        m_HUD.addText("Player4Score", "P4: " + std::to_string(m_playerVector[3].getScore()), "MONOFONT.ttf", 30, score, 1100, 10);
//
//        m_HUD.addText("RoundFinished", "ROUND FINISHED!", "MONOFONT.ttf", 70, score, 450, 360);
//        m_HUD.addText("GameFinished", "GAME FINISHED!", "MONOFONT.ttf", 70, score, 450, 260);
//        m_HUD.addText("Winner", "PLAYER 1 WINS!", "MONOFONT.ttf", 70, score, 450, 360);
//    }
//    else
//    {
        m_HUD.addText("Time", "0", "../Resources/Fonts/MONOFONT.ttf", 30, time, 550, 10);

        SDL_Color score = { 255, 255, 255 };
        m_HUD.addText("Player1Score", "P1: " + std::to_string(m_playerVector[0].getScore()), "../Resources/Fonts/MONOFONT.ttf", 30, score, 20, 10);
        m_HUD.addText("Player2Score", "P2: " + std::to_string(m_playerVector[1].getScore()), "../Resources/Fonts/MONOFONT.ttf", 30, score, 300, 10);
        m_HUD.addText("Player3Score", "P3: " + std::to_string(m_playerVector[2].getScore()), "../Resources/Fonts/MONOFONT.ttf", 30, score, 850, 10);
        m_HUD.addText("Player4Score", "P4: " + std::to_string(m_playerVector[3].getScore()), "../Resources/Fonts/MONOFONT.ttf", 30, score, 1100, 10);

        m_HUD.addText("RoundFinished", "ROUND FINISHED!", "../Resources/Fonts/MONOFONT.ttf", 70, score, 450, 360);
        m_HUD.addText("GameFinished", "GAME FINISHED!", "../Resources/Fonts/MONOFONT.ttf", 70, score, 450, 260);
        m_HUD.addText("Winner", "PLAYER 1 WINS!", "../Resources/Fonts/MONOFONT.ttf", 70, score, 450, 360);
    //}


	m_currentRound = 0;
	m_maxRounds = 3;
}


Game::~Game()
{
}

#pragma endregion

void Game::update()
{
	if (m_servicesManager->getTimeManager()->getTimerElapsed("RoundTime") == 25)
	{
		roundOver();
	}

	m_HUD.setText("Player1Score", "P1: " + std::to_string(m_playerVector[0].getScore()));
	m_HUD.setText("Player2Score", "P2: " + std::to_string(m_playerVector[1].getScore()));
	m_HUD.setText("Player3Score", "P3: " + std::to_string(m_playerVector[2].getScore()));
	m_HUD.setText("Player4Score", "P4: " + std::to_string(m_playerVector[3].getScore()));

	m_HUD.setText("Time", std::to_string(/*(int)60 - */m_servicesManager->getTimeManager()->getTimerElapsed("RoundTime")));
}


void Game::render()
{
	m_renderer->renderTexture(m_servicesManager->getResourceManager()->getTexture("GameBG"), NULL, NULL);

	for (unsigned int index = 0; index < m_platformVector.size(); ++index)
	{
		m_renderer->renderRect(&m_platformVector[index].getRect(), 255, 128, 0, 255);
	}

	for (unsigned int index = 0; index < m_objectiveVector.size(); ++index)
	{
		m_renderer->renderTexture(m_objectiveVector[index].getCurrentTextureName(), NULL, &m_objectiveVector[index].getObjectiveRect());
		m_renderer->renderTexture(m_objectiveVector[index].getIndicatorTexture(), NULL, &m_objectiveVector[index].getIndicatorRect());
	}

	for (unsigned int index = 0; index < m_playerVector.size(); ++index)
	{
		//m_renderer->renderTexture(m_playerVector[index].getCurrentTextureName(), NULL, &m_playerVector[index].getRect());
	}

	m_renderer->renderText(m_HUD.getText("Time"));

	m_renderer->renderText(m_HUD.getText("Player1Score"));
	m_renderer->renderText(m_HUD.getText("Player2Score"));
	m_renderer->renderText(m_HUD.getText("Player3Score"));
	m_renderer->renderText(m_HUD.getText("Player4Score"));

	m_renderer->renderText(m_HUD.getText("GameFinished"));
	m_renderer->renderText(m_HUD.getText("Winner"));
}


void Game::start()
{
	m_currentRound = 1;
	m_servicesManager->getTimeManager()->startNewTimer("RoundTime", 30, true);
}


void Game::roundOver()
{
	m_servicesManager->getTimeManager()->resetTimer("RoundTime");
}
