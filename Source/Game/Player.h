#pragma once

#include <string>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>>
#endif

class Player
{
	/* Constructors & Destructors */
public:
	Player(unsigned int playerID, std::string currentTextureName, unsigned int positionX, unsigned int positionY, unsigned int width, unsigned int height);
	~Player();

public:
	void setCurrentTextureName(std::string currentTextureName);
	std::string getCurrentTextureName();
	SDL_Rect& getRect();
	void setScore(int score);
	int getScore();

private:
	unsigned int m_playerID;
	std::string m_currentTextureName;
	SDL_Rect m_rect;
	int m_score;

};

