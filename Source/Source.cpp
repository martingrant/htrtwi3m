#pragma once

#include "Application/Application.h"

int main(int argc, char *args[])
{
	Application HowToRuleTheWorldIn3Minutes = Application();

	while (HowToRuleTheWorldIn3Minutes.run())
		continue;

	return 0;
}