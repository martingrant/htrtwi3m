#include "ServicesManager.h"


ServicesManager::ServicesManager()
{
	m_inputManager = std::shared_ptr<SDLInput>(new SDLInput());
	m_timeManager = std::shared_ptr<TimeManager>(new TimeManager(30, 5));
}


ServicesManager::~ServicesManager()
{
}


void ServicesManager::registerService(std::shared_ptr<ResourceManager> resourceManager)
{
	m_resourceManager = resourceManager;
}


std::shared_ptr<ResourceManager> ServicesManager::getResourceManager()
{
	return m_resourceManager;
}


void ServicesManager::registerService(std::shared_ptr<SDLInput> inputManager)
{
	m_inputManager = inputManager;
}


std::shared_ptr<SDLInput> ServicesManager::getInputManager()
{
	return m_inputManager;
}


void ServicesManager::registerService(std::shared_ptr<TimeManager> timeManager)
{
	m_timeManager = timeManager;
}


std::shared_ptr<TimeManager> ServicesManager::getTimeManager()
{
	return m_timeManager;
}


void ServicesManager::registerService(std::shared_ptr<ScreenManager> screenManager)
{
	m_screenManager = screenManager;
}


std::shared_ptr<ScreenManager> ServicesManager::getScreenManager()
{
	return m_screenManager;
}