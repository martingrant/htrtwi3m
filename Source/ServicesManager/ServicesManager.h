#pragma once

#include <memory>

#include "../Resources/ResourceManager.h"
#include "../Input/SDLInput.h"
#include "../Time/TimeManager.h"
#include "../Screens/ScreenManager.h"

class ServicesManager
{
public:
	ServicesManager();
	~ServicesManager();

public:
	void registerService(std::shared_ptr<ResourceManager> resourceManager);
	std::shared_ptr<ResourceManager> getResourceManager();

	void registerService(std::shared_ptr<SDLInput> inputManager);
	std::shared_ptr<SDLInput> getInputManager();

	void registerService(std::shared_ptr<TimeManager> timeManager);
	std::shared_ptr<TimeManager> getTimeManager();

	void registerService(std::shared_ptr<ScreenManager> screenManager);
	std::shared_ptr<ScreenManager> getScreenManager();

private:
	std::shared_ptr<ResourceManager> m_resourceManager;
	std::shared_ptr<SDLInput> m_inputManager;
	std::shared_ptr<TimeManager> m_timeManager;
	std::shared_ptr<ScreenManager> m_screenManager;
};

