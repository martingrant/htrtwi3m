#pragma once

#include <memory>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#endif

#include "../Renderer/SDLRenderer.h"
#include "../ServicesManager/ServicesManager.h"
#include "Screen.h"

class MainMenuScreen : public Screen
{
	/* Constructors & Destructors */
public:
	MainMenuScreen(std::shared_ptr<ServicesManager> servicesManager, std::shared_ptr<SDLRenderer> renderer);
	~MainMenuScreen();

	/* General Public Methods */
public:
	virtual void update();
	virtual void render();
	virtual void onActivated();
	virtual void onDeactivated();

private:
	SDL_Rect m_startButtonRect;
	SDL_Rect m_creditsButtonRect;

private:
	std::shared_ptr<ServicesManager> m_servicesManager;
	std::shared_ptr<SDLRenderer> m_renderer;
};

