#include "CreditsScreen.h"


#pragma region Constructors & Destructors

CreditsScreen::CreditsScreen(std::shared_ptr<ServicesManager> servicesManager, std::shared_ptr<SDLRenderer> renderer)
{
	m_servicesManager = servicesManager;
	m_renderer = renderer;

	m_backButtonRect.x = 900;
	m_backButtonRect.y = 150;
	m_backButtonRect.w = 258;
	m_backButtonRect.h = 94;
}


CreditsScreen::~CreditsScreen()
{
}

#pragma endregion

#pragma region General Public Methods

void CreditsScreen::update()
{
	// Check if mouse is clicking on back button
	int mouseX = m_servicesManager->getInputManager()->getMousePosition().first;
	int mouseY = m_servicesManager->getInputManager()->getMousePosition().second;

	if (mouseX > m_backButtonRect.x && mouseX < m_backButtonRect.x + m_backButtonRect.w)
	{
		if (mouseY > m_backButtonRect.y && mouseY < m_backButtonRect.y + m_backButtonRect.h)
		{
			if (m_servicesManager->getInputManager()->mouseButtonState(1) == true)
			{
				m_servicesManager->getScreenManager()->setCurrentScreen("MainMenuScreen");
			}
		}
	}	
}


void CreditsScreen::render()
{
	m_renderer->renderTexture(m_servicesManager->getResourceManager()->getTexture("CreditsBG"), NULL, NULL);
	m_renderer->renderTexture(m_servicesManager->getResourceManager()->getTexture("BackButton"), NULL, &m_backButtonRect);
}


void CreditsScreen::onActivated()
{

}


void CreditsScreen::onDeactivated()
{

}

#pragma endregion

