#pragma once
#pragma once


class Screen
{
public:
	/* Class Constructor & Destructor */
	virtual ~Screen() {}

public:
	/* General Public Methods */
	virtual void update() = 0;
	virtual void render() = 0;
	virtual void onActivated() = 0;
	virtual void onDeactivated() = 0;

};

