#pragma once

#define DEG_TO_RADIAN 0.017453293

#include <stack>
#include <sstream> 

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#endif

#include "Screen.h"
#include "../Input/SDLInput.h"
#include "../ServicesManager/ServicesManager.h"
#include "../Renderer/SDLRenderer.h"

class CreditsScreen : public Screen
{
	/* Constructors & Destructors */
public:
	CreditsScreen(std::shared_ptr<ServicesManager> servicesManager, std::shared_ptr<SDLRenderer> renderer);
	~CreditsScreen();

	/* General Public Methods */
public:
	virtual void update();
	virtual void render();
	virtual void onActivated();
	virtual void onDeactivated();

private:
	SDL_Rect m_backButtonRect;

private:
	std::shared_ptr<ServicesManager> m_servicesManager;
	std::shared_ptr<SDLRenderer> m_renderer;
};

