#pragma once

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
#include <TargetConditionals.h>
#if TARGET_IPHONE_SIMULATOR
#include "SDL.h"
#elif TARGET_OS_MAC
#include <SDL2/SDL.h>
#endif
#endif

#include "Screen.h"
#include "../Input/SDLInput.h"
#include "../ServicesManager/ServicesManager.h"
#include "../Renderer/SDLRenderer.h"
#include "../Game/Game.h"

class GameScreen : public Screen
{
	/* Constructors & Destructors */
public:
	GameScreen(std::shared_ptr<ServicesManager> servicesManager, std::shared_ptr<SDLRenderer> renderer);
	~GameScreen();

	/* General Public Methods */
public:
	virtual void update();
	virtual void render();
	virtual void onActivated();
	virtual void onDeactivated();

private:
	std::shared_ptr<ServicesManager> m_servicesManager;
	std::shared_ptr<SDLRenderer> m_renderer;

private:
	Game m_game;
};

