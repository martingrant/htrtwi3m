#include "GameScreen.h"


#pragma region Constructors & Destructors

GameScreen::GameScreen(std::shared_ptr<ServicesManager> servicesManager, std::shared_ptr<SDLRenderer> renderer)
{
	m_servicesManager = servicesManager;
	m_renderer = renderer;

	m_game = Game(m_renderer, m_servicesManager);
}


GameScreen::~GameScreen()
{
}

#pragma endregion

#pragma region General Public Methods

void GameScreen::update()
{
	m_game.update();
}


void GameScreen::render()
{
	m_game.render();
}


void GameScreen::onActivated()
{
	m_servicesManager->getResourceManager()->stopMusic();
	m_servicesManager->getResourceManager()->playMusic("Music", -1);
	m_game.start();
}


void GameScreen::onDeactivated()
{
	m_servicesManager->getResourceManager()->stopMusic();
}

#pragma endregion
