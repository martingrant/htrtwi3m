#include "MainMenuScreen.h"


#pragma region Constructors & Destructors

MainMenuScreen::MainMenuScreen(std::shared_ptr<ServicesManager> servicesManager, std::shared_ptr<SDLRenderer> renderer)
{
	m_servicesManager = servicesManager;
	m_renderer = renderer;

	m_startButtonRect.x = 300;
	m_startButtonRect.y = 500;
	m_startButtonRect.w = 258;
	m_startButtonRect.h = 94;

	m_creditsButtonRect.x = 700;
	m_creditsButtonRect.y = 500;
	m_creditsButtonRect.w = 258;
	m_creditsButtonRect.h = 94;
}


MainMenuScreen::~MainMenuScreen()
{
}

#pragma endregion

#pragma region General Public Methods

void MainMenuScreen::update()
{
	int mouseX = m_servicesManager->getInputManager()->getMousePosition().first;
	int mouseY = m_servicesManager->getInputManager()->getMousePosition().second;

	// Check if mouse is clicking on credits button
	if (mouseX > m_creditsButtonRect.x && mouseX < m_creditsButtonRect.x + m_creditsButtonRect.w)
	{
		if (mouseY > m_creditsButtonRect.y && mouseY < m_creditsButtonRect.y + m_creditsButtonRect.h)
		{
			if (m_servicesManager->getInputManager()->mouseButtonState(1) == true)
			{
				m_servicesManager->getScreenManager()->setCurrentScreen("CreditsScreen");
			}
		}
	}

	// Check if mouse is clicking on start button
	if (mouseX > m_startButtonRect.x && mouseX < m_startButtonRect.x + m_startButtonRect.w)
	{
		if (mouseY > m_startButtonRect.y && mouseY < m_startButtonRect.y + m_startButtonRect.h)
		{
			if (m_servicesManager->getInputManager()->mouseButtonState(1) == true)
			{
				m_servicesManager->getScreenManager()->setCurrentScreen("GameScreen");
			}
		}
	}
}


void MainMenuScreen::render()
{
	m_renderer->renderTexture(m_servicesManager->getResourceManager()->getTexture("MainMenuBG"), NULL, NULL);
	m_renderer->renderTexture(m_servicesManager->getResourceManager()->getTexture("StartButton"), NULL, &m_startButtonRect);
	m_renderer->renderTexture(m_servicesManager->getResourceManager()->getTexture("CreditsButton"), NULL, &m_creditsButtonRect);
}


void MainMenuScreen::onActivated()
{
	m_servicesManager->getResourceManager()->playMusic("IntroLoop", -1);
}


void MainMenuScreen::onDeactivated()
{
}

#pragma endregion
