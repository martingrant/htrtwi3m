#pragma once

#include <unordered_map>

#ifdef _WIN32
#include <SDL.h>
#elif __APPLE__
    #include <TargetConditionals.h>
    #if TARGET_IPHONE_SIMULATOR
        #include "SDL.h"
    #elif TARGET_OS_MAC
        #include <SDL2/SDL.h>
    #endif
#else /* LINUX */
#include <SDL2/SDL.h>
#endif


#include "Timer.h"

class TimeManager
{
public:
	TimeManager(unsigned int ticksPerSecond, unsigned int maxFrameSkip);
	~TimeManager();

public:
	unsigned int getTickCount();
	unsigned int getNextGameTick();
	unsigned int getNumberOfLoops();
	void incrementNextGameTick();
	void resetNumberOfLoops();
	void incrementNumberOfLoops();
	void setInterpolation();
	float getInterpolation();
	unsigned int getMaxFrameSkip();

public:
	void setConsoleOutTimer();
	bool getConsoleOutTimer();

private:
	int m_consoleOutTime;

private:
	const unsigned int m_ticksPerSecond;
	const unsigned int m_skipTicks;
	const unsigned int m_maxFrameSkip;

	unsigned int m_nextGameTick;
	unsigned int m_numberOfLoops;
	float m_interpolation;

public:
	void update();
	void startNewTimer(std::string name, unsigned int duration, bool countDown);
	unsigned int getTimerElapsed(std::string name);
	void resetTimer(std::string name);

private:
	std::unordered_map<std::string, Timer> m_timerMap;
};

