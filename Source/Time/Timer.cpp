#include "Timer.h"


Timer::Timer(unsigned int duration, bool countDown) : m_duration(duration), m_countDown(countDown)
{
	startTicks = clock();
	m_finished = false;
	m_elapsedTime = 0;
	m_running = false;
}


Timer::~Timer()
{
}


unsigned int Timer::getElapsedTime()
{
	if (m_countDown == true)
	{
		return m_duration - m_elapsedTime;
	}
	else
	{
		return m_elapsedTime;
	}
}


void Timer::update()
{
	if (m_running == false)
	{
		currentTicks = clock();

		ticksTaken = currentTicks - startTicks;

		m_elapsedTime = ((unsigned int)currentTicks - (unsigned int)startTicks / (unsigned int)CLOCKS_PER_SEC) / 1000;

		if (m_elapsedTime >= m_duration)
		{
			m_finished = true;
		}
	}
}


bool Timer::hasFinished()
{
	return m_finished;
}


void Timer::start()
{
	m_running = false;
}


void Timer::stop()
{
	m_running = true;
}


void Timer::reset()
{
	startTicks = clock();
	m_elapsedTime = 0;
	m_finished = false;
	currentTicks = clock();
	ticksTaken = 0;
}


