#pragma once

#include <time.h>
#include <iostream>
#include <chrono>

class Timer
{
public:
	Timer() { }
	Timer(unsigned int duration, bool countDown);
	~Timer();

public:
	unsigned int getElapsedTime();
	void update();
	bool hasFinished();
	void start();
	void stop();
	void reset();

private:
	unsigned int m_duration;
	unsigned int m_elapsedTime;
	bool m_finished;
	bool m_countDown;
	bool m_running;

	clock_t startTicks;
	clock_t currentTicks;
	clock_t ticksTaken;
};

