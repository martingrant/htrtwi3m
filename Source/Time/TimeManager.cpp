#include "TimeManager.h"


TimeManager::TimeManager(unsigned int ticksPerSecond, unsigned int maxFrameSkip) : m_ticksPerSecond(ticksPerSecond), m_skipTicks(1000 / m_ticksPerSecond), m_maxFrameSkip(maxFrameSkip)
{
	m_nextGameTick = getTickCount();
	m_numberOfLoops = 0;
	m_interpolation = 1.0f;
}


TimeManager::~TimeManager()
{
}


unsigned int TimeManager::getTickCount()
{
	return SDL_GetTicks();
}


unsigned int TimeManager::getNextGameTick()
{
	return m_nextGameTick;
}


void TimeManager::resetNumberOfLoops()
{
	m_numberOfLoops = 0;
}


unsigned int TimeManager::getNumberOfLoops()
{
	return m_numberOfLoops;
}


void TimeManager::incrementNextGameTick()
{
	m_nextGameTick += m_skipTicks;
}


void TimeManager::incrementNumberOfLoops()
{
	m_numberOfLoops++;
}


void TimeManager::setInterpolation()
{
	m_interpolation = float(getTickCount() + m_skipTicks - m_nextGameTick) / m_skipTicks;
}


float TimeManager::getInterpolation()
{
	return m_interpolation;
}


unsigned int TimeManager::getMaxFrameSkip()
{
	return m_maxFrameSkip;
}


void TimeManager::setConsoleOutTimer()
{

}


bool TimeManager::getConsoleOutTimer()
{
	return true;
}


void TimeManager::update()
{
	for (auto iterator = m_timerMap.begin(); iterator != m_timerMap.end(); ++iterator)
	{
		iterator->second.update();
	}
}


void TimeManager::startNewTimer(std::string name, unsigned int duration, bool countDown)
{
	m_timerMap[name] = Timer(duration, countDown);
}


unsigned int TimeManager::getTimerElapsed(std::string name)
{
	return m_timerMap[name].getElapsedTime();
}


void TimeManager::resetTimer(std::string name)
{
	m_timerMap[name].reset();
}